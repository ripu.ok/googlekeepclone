import "./header.scss";

const Header = (props) => {
    const logo =(
    <img 
        src="https://www.gstatic.com/images/branding/product/1x/keep_2020q4_48dp.png" 
        alt="keep logo" 
        className="logo"        
    />
    );
  return (
    <div className='header'>
    {logo}
    <h1 >Notes (G Keep Clone)</h1>

    </div>
  )
}

export default Header;