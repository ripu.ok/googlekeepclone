import "./notes.scss";
import DeleteIcon from '@mui/icons-material/Delete';

function Notes({title,content, onDelete,id}) {
  
  
  return (
    <div className="note">        
        <h1>{title}</h1>
        <p>{content}</p>
        <button className="delete" onClick={()=>onDelete(id)}>
        <DeleteIcon />
        </button>
    </div>
  )
}

export default Notes;
