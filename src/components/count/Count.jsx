import "./count.scss"

const Count = ({notes}) => {
  return (
    <div className='count'>
      {!notes.length ? "Add Notes" : 
      `Currently, There are ${notes.length} note in Database.` } 
    </div>
  )
}

export default Count;