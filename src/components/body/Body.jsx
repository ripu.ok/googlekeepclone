import "./body.scss";
import Notes from "../notes/Notes";
import React,{ useState } from "react";
import Count from "../count/Count";
import AddIcon from '@mui/icons-material/Add';

const Body = (props) => {
  const [expanded,setExpanded] = useState(false);

  const [note, setNotes] = useState({
    title:"",
    content:""
  });

  function handleClick(e){
    const {name, value} = e.target;
    //console.log(value);
    setNotes(preValue=>{
      return{
        ...preValue,
        [name]: value       
      };
    });
  }

  const [allNotes,setAllNotes ] = useState([]);

  function storeNote(i){
    // const {name,value} = i.target;
    setAllNotes((preValue,index) =>{
      console.log(preValue);
      console.log(note);
      return[ ...preValue,note]     
      
    }
    )
  }  
  function submitButton(event){
    storeNote(note);
    setNotes({
      title:"",
      content:""
    });
    event.preventDefault();
    setExpanded(false);
  }

  function deleteNote(id){
      setAllNotes(preValue=>{
        return [...preValue.filter((note,index)=>
          index !== id )]
      })

  }
  function expand(){
    setExpanded(true);
  }
  return (
    <div className="body" >
    <Count notes={allNotes}/>
    <form className="form" >
    {console.log(expanded) }
    { expanded && (<input 
        type="text" 
        name="title" 
        placeholder="Title"
        value={note.title}
        onChange={handleClick}
        // onMouseOut={()=>setExpanded(false)}
      />)  }
    <p >
    <textarea onClick={expand}
      name="content" 
      placeholder="Take a note.." 
      value={note.content}
      onChange={handleClick}
      onMouseOver={expand}
      ></textarea>
    </p>
      <button onClick={submitButton} ><AddIcon fontSize="large" /></button>
    </form>

    {allNotes.map((not,index)=>{
      return(
        <Notes
          key={index}
          id={index}
          title={not.title}
          content={not.content}
          onDelete={deleteNote}

        />)
      }
    )}
    

    </div>
  )
}



export default Body;