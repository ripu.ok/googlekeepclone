import "./footer.scss";

const Footer = () => {
    const year = new Date().getFullYear().toString() ;
  return (
    <div className='footer'>
     Ripudaman Singh © {year} 
    </div>
  )
}

export default Footer;